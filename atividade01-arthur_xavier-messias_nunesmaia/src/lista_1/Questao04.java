package lista_1;

import java.util.Scanner;

public class Questao04 {

	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.print("Digite a leta do alfabeto: ");
		String letra = sc.next();
		letra.toLowerCase();
		if (letra.contentEquals("a")||letra.contentEquals("e")||letra.contentEquals("i")||letra.contentEquals("o")||letra.contentEquals("u")) {
			System.out.println("Sua letra � uma vogal");
		}
		else {
			System.out.println("Sua letra � uma consoante");
		}

	}

}
