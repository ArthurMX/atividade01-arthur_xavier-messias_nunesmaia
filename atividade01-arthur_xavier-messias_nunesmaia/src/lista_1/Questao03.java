package lista_1;

import java.util.Scanner;

public class Questao03 {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.print("Digite a sua nota de 0 a 100: ");
		int nota= sc.nextInt();
		String conceito = "";
		if (nota>=0 && nota<=49) {
			conceito="insuficiente";
		}
		else if (nota>=50 && nota<=64) {
			conceito="regular";
		}
		else if (nota>=65 && nota<=84) {
			conceito="boa";
		}
		else if (nota>=85 && nota<=100) {
			conceito="�tima";
		}
		System.out.print("Sua nota � "+conceito);

	}
}
